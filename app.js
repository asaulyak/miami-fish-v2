var express = require('express');

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');


app.use('/images', express.static(__dirname + '/public/images'));
app.use('/public',express.static(__dirname + '/public'));


app.get('/', function (req, res) {
	res.render('index');
});

app.get('/about', function (req, res) {
	res.render('about');
});

app.get('/services', function (req, res) {
	res.render('services');
});

app.get('/products', function (req, res) {
	res.render('products');
});

app.get('/product', function (req, res) {
	res.render('product');
});

app.get('/contacts', function (req, res) {
	res.render('contacts');
});

app.listen(process.env.PORT || 8080);