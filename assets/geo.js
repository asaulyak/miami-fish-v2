$(function() {
   //var mapCanvas = $('.map');

    var center = new google.maps.LatLng(25.767122033097532,-80.20500183105469);


    var mapOptions = {
        center: center,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map($('.map').get(0), mapOptions);

    var marker = new google.maps.Marker({
        position: center,
        map: map,
        draggable: false
    });

});
